//
//  ViewController.swift
//  RecipeApp
//
//  Created by Carissa on 9/27/17.
//  Copyright © 2017 Carissa. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var myrecipeArray: NSArray?
    
    var selectedObject: Dictionary<String, AnyObject> = [:]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (myrecipeArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let obj = myrecipeArray?.object(at: indexPath.row) as! Dictionary<String, AnyObject>
        cell.textLabel?.text = obj["Name"] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedObject = myrecipeArray?.object(at: indexPath.row) as! Dictionary<String, AnyObject>
        performSegue(withIdentifier: "show", sender: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let path = Bundle.main.path(forResource: "recipes", ofType: "plist") {
            myrecipeArray = NSArray(contentsOfFile: path)
        }
        print(myrecipeArray as Any)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let vc = segue.destination as! NextViewController
            vc.info = selectedObject
    }
}

