//
//  NextViewController.swift
//  RecipeApp
//
//  Created by Carissa on 9/27/17.
//  Copyright © 2017 Carissa. All rights reserved.
//

import UIKit

class NextViewController: UIViewController {
    
    @IBOutlet var recipeName: UILabel!
    @IBOutlet var recipeImage: UIImageView!
    @IBOutlet var recipeDesc: UITextView!
    
    var info: Dictionary<String, AnyObject>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
